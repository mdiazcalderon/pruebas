package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class FacturaTest {

	static GestorContabilidad gestor1;
	static Factura factura1;

	//Crea una instancia de gestor una vez al principio de todos los test
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gestor1 = new GestorContabilidad();
	}

	// Me aseguro que antes de cada test solo hay 1 factura en la listaClientes y 
	//no hay ning�n cliente
	@Before
	public void setUp() throws Exception {
		gestor1.getListaFacturas().clear();
		gestor1.getListaClientes().clear();
		factura1 = new Factura("001", LocalDate.parse("2017-10-22"), "Flores", 25, 3);
		gestor1.getListaFacturas().add(factura1);
	}

	//Comprueba que al buscar una factura con dni inexistente devuelve null
	@Test
	public void testBuscarFacturaInexistente() {
		Factura actual = gestor1.buscarFactura("000");
		assertNull(actual);
	}

	//Comprueba que al buscar una factura que si existe devuelve dicha factura
	@Test
	public void testBuscarFacturaExiste() {
		Factura actual = gestor1.buscarFactura("001");
		assertSame(factura1, actual);

	}

	//Comprueba que crea una factura que no existe
	@Test
	public void testCrearFacturaSiNoExiste() {
		Factura nuevaFactura = new Factura("002", LocalDate.parse("2018-01-28"), "Web", 700, 1);
		gestor1.crearFactura(nuevaFactura);

		Boolean seHaCreado = gestor1.getListaFacturas().contains(nuevaFactura);

		assertTrue(seHaCreado);
	}

	//Comprueba que no se crea una factura si ya existe
	@Test
	public void testCrearFacturaSiYaExiste() {
		Factura nuevaFactura = new Factura("001", LocalDate.parse("2017-10-22"), "Flores", 25, 3);
		gestor1.crearFactura(nuevaFactura);

		Boolean seHaCreado = gestor1.getListaFacturas().contains(nuevaFactura);

		assertFalse(seHaCreado);
	}

	//Comprueba cu�l es la factura m�s cara entre 3 facturas
	@Test
	public void testFacturaMasCara() {
		Factura factura2 = new Factura("002", LocalDate.parse("2018-01-11"), "Flores", 25, 4);
		Factura factura3 = new Factura("003", LocalDate.parse("2018-01-11"), "Flores", 25, 1);
		gestor1.getListaFacturas().add(factura2);
		gestor1.getListaFacturas().add(factura3);

		Factura actual = gestor1.facturaMasCara();

		assertEquals(factura2, actual);
	}
	
	//Comprueba que si no hay facturas, el metodo FacturaMasCara devuelve null
	@Test
	public void testFacturaMasCaraSinFacturas() {
		gestor1.getListaFacturas().clear();
		Factura actual = gestor1.facturaMasCara();

		assertNull(actual);
	}

	//Comprueba que la facturaci�n anual es correcta al pasarle un a�o que existe
	@Test
	public void testCalcularFacturacionAnual() {
		Factura factura2 = new Factura("002", LocalDate.parse("2017-01-11"), "Flores", 25, 4);
		Factura factura3 = new Factura("003", LocalDate.parse("2018-01-11"), "Flores", 25, 1);
		gestor1.getListaFacturas().add(factura2);
		gestor1.getListaFacturas().add(factura3);

		float facturacionAnual = gestor1.calcularFacturacionAnual(2017);

		assertEquals(175, facturacionAnual, 0);
	}

	//Comprueba que la facturaci�n anual es 0 si se pasa un a�o que no existe en las facturas
	@Test
	public void testCalcularFacturacionAnualSiNoHayAnio() {
		float facturacionAnual = gestor1.calcularFacturacionAnual(2016);

		assertEquals(0, facturacionAnual, 0);
	}

	@Test
	public void eliminarFacturaSiExiste() {
		gestor1.eliminarFactura("001");

		assertFalse(gestor1.getListaFacturas().contains(factura1));

	}

	// Comprueba si se asigna correctamente un cliente que existe a una factura que existe
	@Test
	public void asignarClienteAFactura() {
		Cliente cliente1 = new Cliente("Maria", "111z", LocalDate.parse("2018-03-15"));
		gestor1.getListaClientes().add(cliente1);
		gestor1.asignarClienteAFactura("111z", "001");
		
		Cliente actual = factura1.getCliente();
		
		assertEquals(cliente1, actual);
		
	}
	
	//Comprueba si no se asigna un cliente cuando el codigo de la factura pasado no existe
		@Test
		public void asignarClienteAFacturaQueNoExiste() {
			Cliente cliente1 = new Cliente("Carlos", "111b", LocalDate.parse("2018-03-15"));
			gestor1.getListaClientes().add(cliente1);
			gestor1.asignarClienteAFactura("111b", "009");
			
			double cantidadFacturas = 0;
			for(int i=0; i<gestor1.getListaFacturas().size(); i++) {
				if(gestor1.getListaFacturas().get(i).getCliente() != null) {
					if(gestor1.getListaFacturas().get(i).getCliente().equals(cliente1)) {
					cantidadFacturas++;
					}
				}
			}
			assertEquals(0,cantidadFacturas,0);
			
		}
		//Comprueba si no se asigna un cliente a una factura cuando el dni de cliente pasado no existe
		@Test
		public void asignarClienteQueNoExisteAFactura() {
			gestor1.asignarClienteAFactura("72996225k", "001");
			
			Cliente actual = factura1.getCliente();
			
			assertNull(actual);
		}
		
		//Comprueba que el calculo cantidad de facturas por cliente se realiza correctamente
		//si el cliente existe y tiene facturas
		@Test
		public void cantidadFacturasPorCliente() {
			Cliente cliente1 = new Cliente("Maria", "111z", LocalDate.parse("2018-03-15"));
			gestor1.getListaClientes().add(cliente1);
			Factura facturaNueva = new Factura("100", LocalDate.parse("2018-04-18"), "Raton",20, 1, cliente1);
			gestor1.getListaFacturas().add(facturaNueva);
			
			double actual = gestor1.cantidadFacturasPorCliente("111z");
			assertEquals(1, actual,0);
			
		}
		
		//Comprueba que la cantidad de facturas por cliente es 0 si no est� asignado a ninguna factura
		@Test
		public void cantidadFacturasPorClienteSinFacturas() {
			Cliente cliente1 = new Cliente("Maria", "111z", LocalDate.parse("2018-03-15"));
			gestor1.getListaClientes().add(cliente1);
			
			double actual = gestor1.cantidadFacturasPorCliente("111z");
			assertEquals(0, actual,0);
			
		}
	
}

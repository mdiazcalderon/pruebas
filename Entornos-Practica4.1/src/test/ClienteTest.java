package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class ClienteTest {

	static Cliente cliente1;
	static GestorContabilidad gestor1;
	
	//Se crea una instancia de GestorContabilidad una vez, antes de todos los test.
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gestor1 = new GestorContabilidad();
		
	}
	
	// Me aseguro que antes de cada test solo hay 1 cliente en la listaClientes
	@Before
	public void setUp(){
		   gestor1.getListaClientes().clear();
		   cliente1 = new Cliente("Maria", "111z", LocalDate.parse("2018-03-15"));
		   gestor1.getListaClientes().add(cliente1);
	}
	
	//Comprueba que el metodo buscarCliente devuelve null si no existe
	@Test
	public void testBuscarClienteInexistente(){
		Cliente actual = gestor1.buscarCliente("72996225K");
		assertNull(actual);
	}

	//Comprueba que el metodo buscarCliente lo busca correctamente
	@Test
	public void testBuscarClienteExiste(){
		Cliente actual = gestor1.buscarCliente("111z");
		assertSame(cliente1, actual);
		
	}
	
	//Comprueba que si un cliente no existe se da de alta
	@Test
	public void testAltaClienteSiNoExiste() {
		Cliente nuevoCliente = new Cliente("Pedro", "999a", LocalDate.parse("2018-05-05"));
		gestor1.altaCliente(nuevoCliente);
		
		Cliente actual = null;
		for(int i=0; i<gestor1.getListaClientes().size(); i++) {
			if(gestor1.getListaClientes().get(i).getDni().equals("999a")) {
				actual = gestor1.getListaClientes().get(i);
			}
		}
		assertSame(nuevoCliente, actual);
	}
	
	//Comprueba que si un cliente ya existe no se da de alta
	@Test
	public void testAltaClienteSiYaExiste() {
		Cliente nuevoCliente = new Cliente("Maria", "111z", LocalDate.parse("2018-05-05"));
		gestor1.altaCliente(nuevoCliente);
		
		Boolean contieneNuevoCliente = gestor1.getListaClientes().contains(nuevoCliente);
		
		assertFalse(contieneNuevoCliente);
	}
	
	//Comprueba que si hay clientes, busca el m�s antiguo correctamente.
	@Test
	public void testClienteMasAntiguoSiHayClientes() {
		Cliente cliente2 = new Cliente("Carlos", "222n", LocalDate.parse("2018-02-19"));
		Cliente cliente3 = new Cliente("Juan", "123a", LocalDate.parse("2018-05-05"));
		gestor1.getListaClientes().add(cliente1);
		gestor1.getListaClientes().add(cliente2);
		gestor1.getListaClientes().add(cliente3);
		
		Cliente actual = gestor1.clienteMasAntiguo();
		
		assertSame(cliente2, actual);
	}
	
	//Comprueba que si no hay clientes, no devuelve ning�n cliente (devuelve null)
	
	@Test
	public void testClienteMasAntiguoSiNoHayClientes() {
		gestor1.getListaClientes().clear();
		Cliente actual = gestor1.clienteMasAntiguo();
		
		assertNull(actual);
	}
	
	//Comprueba que no elimina un cliente que tiene factura asignada
	@Test
	public void testEliminarClienteConFactura() {
		Factura factura1 = new Factura("mcdc", LocalDate.parse("2017-10-22"), "Flores", 125, 3,
				cliente1);
		gestor1.getListaFacturas().add(factura1);
		
		gestor1.eliminarCliente("111z");
		
		assertTrue(gestor1.getListaClientes().contains(cliente1));
		
	}
	
	//Comprueba que elimina un cliente si no tiene factura asignada
	@Test
	public void testEliminarClientes() {
		gestor1.eliminarCliente("111z");
		assertFalse(gestor1.getListaClientes().contains(cliente1));
		
	}
	
}
